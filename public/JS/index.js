
const tabBlock = document.querySelector('.tabs');
const tabArr = document.querySelectorAll('.tabs-title')
const contentArr = document.querySelectorAll('.content-tabs-oue-services')

tabBlock.addEventListener('click', (event) => {
   if (event.target.tagName === 'LI') {
      tabArr.forEach(element => {
         element.classList.remove('tabs-active');
      });
      let tabArticle = event.target.innerHTML;
      event.target.classList.add('tabs-active');
      contentArr.forEach(element => {
         element.hidden = true;
         if (tabArticle === element.dataset.name)
            element.hidden = false;
      });
   }
});

const button = document.querySelector(".button-load-more");
const img = document.querySelector(".photo-item-gallery-load-more");
button.addEventListener("click", (event) => {
   button.classList.add("button-load-more-display-none");
   spinner.classList.remove("display-none");
   setTimeout(hideSpinner, 2000);
});


const spinner = document.querySelector(".loader");
const block = document.querySelector(".content-our-amazing-work-loader");
function hideSpinner() {
   block.classList.add("display-none");
   spinner.classList.add("display-none");
   img.classList.remove("photo-item-gallery-load-more");
}

const tabBlockOur = document.querySelector('.tabs-our');
const tabArrOur = document.querySelectorAll('.tabs-title-our')
const contentArrOur = document.querySelectorAll('.photo-item')

tabBlockOur.addEventListener('click', (event) => {
   if (event.target.tagName === 'LI') {
      tabArrOur.forEach(element => {
         element.classList.remove('tabs-title-our-active');
      });
      let tabArticle = event.target.innerHTML;
      event.target.classList.add('tabs-title-our-active');
      contentArrOur.forEach(element => {
         element.classList.remove("display-none");
         if (tabArticle !== element.dataset.name)
            element.classList.add("display-none");
         if (tabArticle === 'All') {
            element.classList.remove("display-none");
         }
      });
   }
});

$(document).ready(function () {
   $('.slider-for').slick({
      arrows: false,
      fade: true,
      asNavFor: ".slider-nav",

   });
});

$(document).ready(function () {
   $('.slider-nav').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      speed: 500,
      asNavFor: ".slider-for",
      centerMode: true,
      centerPadding: '5px',
      focusOnSelect: true,
   });
});
